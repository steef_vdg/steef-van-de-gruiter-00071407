#Opdrachten

## Opdracht 1

* Het lijkt erop dat de bestellingen niet naar de database worden verstuurd, leg uit hoe dit komt en los het op.
* Op dit moment kan er ook tekst worden ingevoerd bij de hoeveelheid plaatjes, zorg dat dit alleen nog nummers kunnen zijn
* Er moet minimaal een totaal aantal van 10 plaatjes worden besteld. Zorg ervoor dat als de gebruiker te weinig besteld de knop niet ingedrukt kan worden, en dat er een error message komt die aangeeft dat er minimaal 10 plaatjes moeten worden besteld. Deze moet weg gaan als er wel meer dan plaatjes besteld worden.
* Zorg ervoor dat de gebruiker wordt doorverwezen naar de completed.php pagina als de order sucessvol is verzonden.
* Op dit moment zijn er nog geen checks aanwezig voor de data die de gebruiker meestuurt. Dit is natuurlijk niet handig. Zorg dat de eerder genoemde hoeveelheden ook server-side gechecked worden. 
* Er is ook een Voorraad tabel, zorg ervoor dat als de gebruiker besteld, de hoeveelheid die hij invoert van de voorraad afgehaald wordt.
* Als de gebruiker meer probeert de bestellen dan de voorraad, moet de gebruiker ook een melding krijgen, en moet de knop ook niet in te drukken zijn.

## Opdracht 2

* In de database staan ook top 100 nummers opgeslagen.
* Maak een nieuwe pagina die op dezelfde manier is ingedeeld als de anderen, maar hierin een input veld waar een gebruiker een jaar kan invoeren, waarmee de top 100 van dat jaar wordt gegeven.
* De lijst moet worden gesorteerd van nummer 1 naar 100.
* Deze pagina moet in de navigatie balk rechts van Bestellen komen
* Maak ook een ajax call, die een bericht aangeeft, als er voor dat jaar geen top 100 is.
* Dit bericht moet alleen worden laten zien als er 4 of meer nummers zijn ingevoerd.
* Zorg er nu voor, dmv jquery dat als de gebruiker op een rij klikt deze dan verdwijnt van de pagina.
* Zorg ervoor dat de rijen om en om een andere kleur hebben. Dus nummer 1,3,5 etc hebben dezelfde achtergrond kleur en de nummers 2,4,6 hebben ook dezelfde achtergrond kleur.

## Opdracht 3
* Log in op de pagina dmv een sql injectie, zonder een gebruikers naam in te voeren
* Log in op de pagina dmv een sql injectie, maar wel met een werkende gebruikers naam.

## Opdracht 4
* Verander de hoogte van het achtergrond plaatje in de header naar 100px.
* Maak het font van alle menu items Impact.
* Zorg dat als je muis over 1 van de menu items komt, de achergrond hiervan 'standaard' grijs wordt, en de.
* Zorg dat de minimaale hoogte van het hoofd gedeelte van de website altijd 200px is.
* De hoogte moet dus wel grooter worden als er meer content is.

Dit is een test pull request.